package com.example.demo;

import com.example.demo.models.Client;
import com.example.demo.models.Item;
import com.example.demo.models.Order;
import com.example.demo.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.*;

@Component
public class RepositoryTest implements ApplicationRunner {

    private static final long ID_CLIENT_FERNANDO = 11L;
    private static final long ID_CLIENT_ZE_PEQUENO = 22L;

    private static final long ID_ITEM1 = 100L;
    private static final long ID_ITEM2 = 101L;
    private static final long ID_ITEM3 = 102L;

    private static final long ID_ORDER_1 = 1000L;
    private static final long ID_ORDER_2 = 1001L;
    private static final long ID_ORDER_3 = 1002L;

    private final ClientRepository clientRepository;

    @Autowired
    public RepositoryTest(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) {
        out.println(">>> Iniciando carga de dados...");
        Client fernando = new Client(ID_CLIENT_FERNANDO,
                "Fernando Boaglio", "Sampa");
        Client zePequeno = new Client(ID_CLIENT_ZE_PEQUENO,
                "Zé Pequeno", "Cidade de Deus");

        Item dog1 = new Item(ID_ITEM1, "Green Dog tradicional", 25d);
        Item dog2 = new Item(ID_ITEM2, "Green Dog tradicional picante", 27d);
        Item dog3 = new Item(ID_ITEM3, "Green Dog max salada", 30d);

        List<Item> listaOrderFernando1 = new ArrayList<>();
        listaOrderFernando1.add(dog1);

        List<Item> listaOrderZePequeno1 = new ArrayList<>();
        listaOrderZePequeno1.add(dog2);
        listaOrderZePequeno1.add(dog3);

        Order orderDoFernando = new Order(ID_ORDER_1, fernando,
                listaOrderFernando1, dog1.getPrice());
        fernando.addOrder(orderDoFernando);

        Order orderDoZepequeno = new Order(ID_ORDER_2, zePequeno,
                listaOrderZePequeno1, dog2.getPrice() + dog3.getPrice());
        zePequeno.addOrder(orderDoZepequeno);

        out.println(">>> Order 1 - Fernando : " +
                orderDoFernando);
        out.println(">>> Order 2 - Ze Pequeno: " +
                orderDoZepequeno);

        clientRepository.saveAndFlush(zePequeno);
        out.println(">>> Gravado Client 2: " + zePequeno);

        List<Item> listaOrderFernando2 = new ArrayList<>();
        listaOrderFernando2.add(dog2);
        Order order2Dofernando = new Order(ID_ORDER_3, fernando,
                listaOrderFernando2, dog2.getPrice());
        fernando.addOrder(order2Dofernando);
        clientRepository.saveAndFlush(fernando);
        out.println(">>> Order 2-Fernando:" + order2Dofernando);
        out.println(">>> Gravado Client 1: " + fernando);
    }
}
