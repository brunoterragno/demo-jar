package com.example.demo.controllers;

import com.example.demo.models.Client;
import com.example.demo.repositories.ClientRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/clients")
public class ClientController {
    private final ClientRepository clientRepository;

    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @GetMapping("/")
    public List<Client> getClients() {
        return this.clientRepository.findAll();
    }

    @GetMapping("{id}")
    public Optional<Client> getClients(@PathVariable long id) {
        return this.clientRepository.findById(id);
    }
}
