package com.example.demo.models;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Length(min = 2, max = 30, message = "O tamanho do name deve ser entre {min} e {max} caracteres")
    private String name;

    @NotNull
    @Length(min = 2, max = 300, message = "O tamanho do name deve ser entre {min} e {max} caracteres")
    private String address;

    @OneToMany(mappedBy = "client", fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    private List<Order> orders;

    public Client() {
    }

    public Client(Long id, String name, String address) {
        super();
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;

    }

    public void addOrder(Order order) {
        if (this.orders == null) orders = new ArrayList<>();
        orders.add(order);
    }
}
