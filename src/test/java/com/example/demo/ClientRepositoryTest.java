package com.example.demo;

import com.example.demo.models.Client;
import com.example.demo.repositories.ClientRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientRepositoryTest {

    @Autowired
    ClientRepository repository;

    @Test
    public void findClients() {
        Page<Client> clients = this.repository.findAll(new PageRequest(0, 10));
        assertThat(clients.getSize()).isGreaterThan(1);
    }
}